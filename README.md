# tiny-import
###### tiny-import是一个搭配webpack使用的模块引入插件，按照需求一键引入想要的模块

## 示例
文件目录

```
|-- dir
    |-- a.js
    |-- b.js
    |-- c.js
```
```js
import { tinyImport } from 'tiny-import'

const requireList = tinyImport.importAll({
      context: require.context('./dir'),
      include: ['a', 'b']
})

```
requireList 等于
```
{
	a: {},
	b: {} 
}
```

## 原型

### TinyImport
```

import { TinyImport } from 'tiny-import'

const tinyImport = new TinyImport({
	exclude: 'index'     // 全局排除所有index文件，例如index.js index.jpg
})

const requireList = tinyImport.importAll({
      context: require.context('./dir),
      include: ['a', 'b']
})
```
#### 原型参数（globalFilter）
用于定义全局的筛选器, 可选的参数有 { exclude, include, excludePath, includePath }

##### exclude
排除某些文件名的文件（不包含文件后缀）
- type
	- String  排除文件名等于该字符串的文件
	- Function(name, path) 返回Boolean类型 返回true表示排除，false不排除
		- name表示文件名称 path表示文件路径 
	- RegExp 排除匹配文件名
	- Array<String Function RegExp> 可以是一个包含多种条件的数组，任意一个匹配了就会排除
- default: () => false

##### include
引入某些文件名的文件（不包含文件后缀）
- type
	- String  引入文件名等于该字符串的文件
	- Function(name, path) 返回Boolean类型 返回true表示引入，false不引入
		- name表示文件名称 path表示文件路径 
	- RegExp 排除匹配文件名
	- Array<String Function RegExp> 可以是一个包含多种条件的数组，数组所有项都匹配了才会引入
- default: () => true

##### excludePath | includePath
排除 | 引入匹配文件路径的文件（包含文件后缀）

#### 处理顺序
exclude(Path)和include(Path)可以同时使用，include(Path)具有优先级，即先引入，再排除
```
// 引入dir文件夹下的所有js文件，但是排除a.js
import { tinyImport } from 'tiny-import'

const requireList = tinyImport.importAll({
      context: require.context('./dir'),
      include: /.js$/,
	  exclude: 'a',
})
```

### tinyImport
如果只需要引用一次，不需要全局筛选器，则可以直接使用tinyImport

tinyImport的默认规则：
- 排除所有index为文件名的文件，例如index.js index.png
- 排除所有以_或者:开头的文件，例如_index.js :index.js等

```
import { tinyImport } from 'tiny-import'

const requireList = tinyImport.importAll({
      context: require.context('./dir),
      include: ['a', 'b']
})
```


#### 实例方法
##### importAll
###### 参数
- context: **必填** 引入的文件上下文，希望接受一个require.context()的值，详情查看

- exclude(Path) | include(Path): **选填** 和globleFilter一样， 表示当前引入的筛选器

- key({ name, path })
	- **选填** Funtion 在exlude和include匹配前调用，用于修改所有文件的文件名
	- name表示文件的原始文件名
	- path表示文件目录

```
key 方法使用示例
目录
|-- dir
    |-- a.js
    |-- b.js
    |-- c.js
	
import { tinyImport } from 'tiny-import'

const requireList = tinyImport.importAll({
      context: require.context('./dir'),
	  key: ({ name }) => name + name,     // 将所有key变成key+key的类型
	  exclude: 'cc'   // 注意，key方法的调用在exclude之前，所以如果要排除c.js,需要写'cc'
})

{
	aa: {},
	bb: {} 
}
```

- value({ file, path, name, key })
	- **选填** Funtion 在exlude和include匹配前调用，用于修改所有文件的内容
	- file表示文件内容
	- name表示文件的原始文件名
	- path表示文件目录
	- key表示经过key函数处理过的文件名

```
value 方法使用示例
目录
|-- dir
    |-- a.js
    |-- b.js
    |-- c.js
	
import { tinyImport } from 'tiny-import'

const requireList = tinyImport.importAll({
      context: require.context('./dir'),
	  value: ({ file }) => [file],     // 每项都变成数组
})

{
	a: [{}],
	b: [{}],
	c: [{}]
}
```

- noSub: **选填** Boolean 是否引入子文件夹的内容，默认是false

##### importAllType
###### 参数

- context
- exclude
- include
- excludePath
- key
- value
- noSub
*注意，没有includePath参数，该参数会被覆盖*

- fileType: 文件后缀名 String

```
// 例如要引入所有png类型的文件
importAllType({
    context: require.context('./dir'),
    fileType: 'png'
})
```

预设的其他类型引入():

- importJs  // 引入js类型
- importImg // 引入图片
- importVue // 引入vue组件
- importJson // 引入json
- importTs // 引入ts文件

参数
- context
- exclude
- include
- excludePath
- key
- value
- noSub

### 如何拓展其他类型?

```
// 例如想要定义一个方法，用于引入所有的.mbx文件

class TinyImport2 extends TinyImport{
    importMbx(params) {
        return this.importAllType({
            ...params,
            fileType: 'mbx',
        })
    }
}
```

###下个版本想做的内容？
- 引入结果按文件目录返回
- inportAllType支持includePath参数


## routeImport
专为vue-router提供的插件，根据路由文件夹目录生成[vue-router](https://router.vuejs.org/zh/ "vue-router")可用的路由表

我们通过给vue文件取特定格式的名称来指定路由类型，这一灵感来自于[nuxt.js](https://zh.nuxtjs.org/guide/routing/ "nuxt route")，但是略有不同

###使用示例
```
// 目录
|-- page
      |-- ChildA.vue
      |-- ChildB
      		|-- index.vue
```

```
// router.js
import Vue from 'vue'
import VueRouter from 'vue-router'

import { routeImport } from 'tiny-import'

Vue.use(VueRouter)

const routes = routeImport.importRoute({
  context: require.context('../views'),
})

const router = new VueRouter({
  routes,
})

export default router
```

```
// 路由
[
    {
        "path":"/childa",
        "name":"ChildA",
        "component":{
            "name":"ChildA",
            "components":{},
        },
        "children":[],
    },
    {
        "path":"/childb",
        "name":"ChildB",
        "component":{
            "name":"ChildB",
            "components":{ },
        },
        "children":[],
    }
]

```

### 实例方法

##### importRoute
引入路由
##### 参数

- context 必填
- root 选填 表示根目录使用哪个路由


```
// router.js
// 当匹配到/根目录时，显示ChildA.vue
const routes = routeImport.importRoute({
  context: require.context('../views'),
  root: 'ChildA'
})
```

```
// 路由
[
	{
        "path":"/",
        "name":"/",
        "component":{
            "name":"ChildA",
            "components":{},
        },
        "children":[],
    },
    {
        "path":"/childa",
        "name":"ChildA",
        "component":{
            "name":"ChildA",
            "components":{},
        },
        "children":[],
    },
    {
        "path":"/childb",
        "name":"ChildB",
        "component":{
            "name":"ChildB",
            "components":{ },
        },
        "children":[],
    }
]

```

- include 选填 不建议填写
- excludePath 选填 不建议填写
- noSub 选填 不建议填写

##### 普通具名式路由([官方文档](https://router.vuejs.org/zh/guide/essentials/named-routes.html "官方文档"))
没有任何特殊符号作为文件名的路由，例如ChildA, ChildB, PageA等
推荐使用大写字母开头，驼峰式，路由的path会被转换为小写字母，例如childa, childb, pagea

##### 动态匹配式路由([官方文档](https://router.vuejs.org/zh/guide/essentials/dynamic-matching.html#%E5%93%8D%E5%BA%94%E8%B7%AF%E7%94%B1%E5%8F%82%E6%95%B0%E7%9A%84%E5%8F%98%E5%8C%96 "官方文档"))
以:符号开头的路由，例如:DynamicB

这样，任意访问/123 的路由将会匹配到:DynamicB.vue,并且注入this.$route.params = { "dynamicb": "123"  }


```
// 目录
|-- page
      |-- ChildA.vue
      |-- :id#DynamicB.vue
```

```
// 路由
[
    {
        "path":"/childa",
        "name":"ChildA",
        "component":{
            "name":"ChildA",
            "components":{},
        },
        "children":[],
    },
    {
        "path":"/:dynamicb",
        "name":"DynamicB",
        "component":{
            "name":"DynamicB",
            "components":{ },
        },
        "children":[],
    }
]

```

###### 指定动态参数
以:符号开头，含有#号的路由，例如:id#DynamicC.vue

这样，任意访问/123 的路由将会匹配到:id#DynamicC.vue,并且注入this.$route.params = { "id": "123"  }

```
// 目录
|-- page
      |-- ChildA.vue
      |-- :id#DynamicC.vue
```

```
// 路由
[
    {
        "path":"/childa",
        "name":"ChildA",
        "component":{
            "name":"ChildA",
            "components":{},
        },
        "children":[],
    },
    {
        "path":"/:id",
        "name":"DynamicC",
        "component":{
            "name":"DynamicC",
            "components":{ },
        },
        "children":[],
    }
]

```


##### 捕获式路由([官方文档](https://router.vuejs.org/zh/guide/essentials/dynamic-matching.html#%E6%8D%95%E8%8E%B7%E6%89%80%E6%9C%89%E8%B7%AF%E7%94%B1%E6%88%96-404-not-found-%E8%B7%AF%E7%94%B1"官方文档"))
以@命名的路由，即@.vue，当没有任何路由匹配到时，会匹配到这个组件，一般用来设计404页面

```
// 目录
|-- page
      |-- ChildA.vue
      |-- ChildB.vue
	  |-- @.vue
```

```
// 路由
[
    {
        "path":"/childa",
        "name":"ChildA",
        "component":{
            "name":"ChildA",
            "components":{},
        },
        "children":[],
    },
    {
        "path":"/childb",
        "name":"ChildB",
        "component":{
            "name":"ChildB",
            "components":{ },
        },
        "children":[],
    },
	{
        "path":"*",
        "name":"@",
        "component":{
            "name":"@",
            "components":{ },
        },
        "children":[],
    }
]

```


##### 嵌套路由([官方文档](https://router.vuejs.org/zh/guide/essentials/nested-routes.html "官方文档"))
以文件夹的关系来表示嵌套关系

现在，访问/childb/subchilda将会匹配到SubchildA.vue
*记得在ChildB组件内添加<route-view></route-view>路由根节点*

```
// 目录
|-- page
      |-- ChildA.vue
      |-- ChildB
		|-- index.vue
		|-- SubChildA.vue
```

```
// 路由
[
    {
        "path":"/childa",
        "name":"ChildA",
        "component":{
            "name":"ChildA",
            "components":{},
        },
        "children":[],
    },
    {
        "path":"/:dynamicb",
        "name":"DynamicB",
        "component":{
            "name":"DynamicB",
            "components":{ },
        },
        "children":[
		     {
                "path":"subchilda",
                "name":"SubChildA",
                "component":{
                    "name":"SubChildA",
                    "components":{},
                },
                "children":[

                ],
            }
		],
    }
]

```



#### importComponent
我们推荐把公用组件提取出，单独放在src/component文件夹下
但是仍然有某些组件，只在某些页面使用，我们把他称为该页面的私有组件

在tiny-import中，使用_开头的vue文件会被当做该文件夹下的私有组件，我们提供了importComponent方法，专门用于引入这些私有组件

##### 参数

- context 必填
- exclude 选填
- excludePath 选填 不建议填写

##### 使用示例
```
// 目录
|-- page
      |-- ChildA.vue
      |-- ChildB
		|-- index.vue
		|-- SubChildA.vue
		|-- _ComponentA.vue
		|-- _ComponentB.vue
```

```
// 该_ComponentA(B)不会被添加到路由表中
// 在page/ChildB/index.vue中引入这些组件
import { routeImport } from 'tiny-import'

const components = routeImport.importComponent({
  context: require.context('./')
})

export default {
  name: 'ChildB',
  components,  // 注入组件
  data() {
    return {}
  },
  watch: {},
  computed: {},
  created() {},
  methods: {}
}

// components 等于 :
{
	ComponentA: {
		name: "ComponentA",
		components: { },
	},
	ComponentB: {
		name: "ComponentB",
		components: { },
	}
}

```


### 一个含有所有Api的例子

```
|-- page
      |-- :id#DynamicA.vue
      |-- @.vue
      |-- ChildA.vue
      |-- ChildB
      |   |-- _ComponentB.vue
      |   |-- index.vue
      |   |-- _ComponentA
      |       |-- _ComponentC.vue
      |       |-- index.vue
      |-- ChildC
          |-- :id#DynamicSubChildC.vue
          |-- @.vue
          |-- SubChildA.vue
          |-- SubChildB.vue
          |-- index.vue
```


```
// 路由表
[
    {
        "path": "/",
        "name": "/",
        "component": {}
    },
    {
        "path": "/childa",
        "name": "ChildA",
        "component": {},
        "children": []
    },
    {
        "path": "/childb",
        "name": "ChildB",
        "component": {},
        "children": []
    },
    {
        "path": "/childc",
        "name": "ChildC",
        "component": {},
        "children": [
            {
                "path": "subchilda",
                "name": "SubChildA",
                "component": {},
                "children": []
            },
            {
                "path": "subchildb",
                "name": "SubChildB",
                "component": {},
                "children": []
            },
            {
                "path": ":id",
                "name": "DynamicSubChildC",
                "component": {},
                "children": []
            },
            {
                "path": "*",
                "name": "@",
                "component": {},
                "children": []
            }
        ]
    },
    {
        "path": "/:dynamicb",
        "name": "DynamicB",
        "component": {},
        "children": []
    },
    {
        "path": "/:id",
        "name": "DynamicA",
        "component": {},
        "children": []
    },
    {
        "path": "*",
        "name": "@",
        "component": {},
        "children": []
    }
]
```

### 下一个版本要实现的
- 更直观更明智的文件标识符设计
- 允许开发者自定义文件标示符
- 允许在vue文件内部定义路由类型，而不是在文件名上
- 在文件夹的index.vue中自动引入私有组件，而不用手动引入它


## 希望实现的内容
- 将整个模块做成webpack的一个loader
- 摆脱require.context依赖，可以在node中使用
- 用.tiignore或者ti.config.js等文件代替繁琐的exclude(include)


## License (MIT)

Copyright (c) 2020 Leo

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.












