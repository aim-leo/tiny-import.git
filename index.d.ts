export interface TinyImport {
  new (params?: GlobalFilter): any
  importAll(params: importAllParams): Object
  importAllType(params: typedFileParams): Object
  importJs(params: NoIncludeParams): Object
  importImg(params: NoIncludeParams): Object
  importVue(params: NoIncludeParams): Object
  importJson(params: NoIncludeParams): Object
  importTs(params: NoIncludeParams): Object
  importRoute(params: NoIncludeParams): Array<RouteTypedResult>
  importComponent(params: NoIncludeParams): Object
}

export interface GlobalFilter {
  exclude?: string | RegExp | string[] | RegExp[] | Function
  include?: string | RegExp | string[] | RegExp[] | Function
}

export interface importAllParams extends GlobalFilter {
  context: Function
  key?: string | Function
  value?: string | Function
}

export interface NoIncludeParams {
  context: Function
  key?: string | Function
  value?: string | Function
  exclude?: string | RegExp | string[] | RegExp[] | Function
}

export interface typedFileParams extends NoIncludeParams {
  fileType: string
}

export interface RouteTypedResult {
  component: Object
  name: string
  path: string
}

declare const tinyImport: TinyImport
