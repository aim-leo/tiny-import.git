import requireContext from 'require-context.macro'
import { tinyImport as eq } from '../../src/index'

describe('importImg', () => {
  test('require dir, check img', () => {
    const requireList = eq.importImg({
      context: requireContext('./dir', false),
    })

    expect(requireList).toMatchObject({
      a: 'test-file-stub',
      b: 'test-file-stub',
    })
  })
})
