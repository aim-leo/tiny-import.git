import requireContext from 'require-context.macro'
import { tinyImport as eq } from '../../src/index'

describe('importTs', () => {
  test(`importTs export, get the export`, () => {
    const requireList = eq.importTs({
      context: requireContext('./', true),
    })

    expect(requireList).toEqual({ a: { default: { a: '' } } })
  })
})
