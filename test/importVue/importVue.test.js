import requireContext from 'require-context.macro'
import { tinyImport as eq } from '../../src/index'

describe('importVue', () => {
  test('require vue-dir dir, check vue content', () => {
    const requireList = eq.importVue({
      context: requireContext('./dir', false),
    })

    expect(requireList).toMatchObject({
      a: {
        name: 'a',
        components: {},
        watch: {},
        computed: {},
        methods: {},
      },
      b: {
        name: 'b',
        components: {},
        watch: {},
        computed: {},
        methods: {},
      },
    })
  })
})
