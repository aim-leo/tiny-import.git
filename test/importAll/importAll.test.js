import requireContext from 'require-context.macro'
import { tinyImport as eq } from '../../src/index'

describe('importAll', () => {
  test('require empty dir', () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/empty', true, /.js*/),
    })
    expect(requireList).toStrictEqual({})
  })

  test('require one-file dir', () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/one-file', true, /.js*/),
    })
    expect(Object.keys(requireList)).toStrictEqual(['a'])
  })

  test('require many-file dir', () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/many-file', true, /.js*/),
    })
    expect(Object.keys(requireList)).toStrictEqual(['a', 'b', 'c'])
  })

  test(`require many-file dir, include: ['a']`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/many-file', true, /.js*/),
      include: ['a'],
    })
    expect(Object.keys(requireList)).toStrictEqual(['a'])
  })

  test(`require many-file dir, include: ['a', 'b']`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/many-file', true, /.js*/),
      include: ['a', 'b'],
    })
    expect(Object.keys(requireList)).toStrictEqual(['a', 'b'])
  })

  test(`require many-file dir, include: ['a', 'b'], and exclude ['b']`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/many-file', true, /.js*/),
      include: ['a', 'b'],
      exclude: ['b'],
    })
    expect(Object.keys(requireList)).toStrictEqual(['a'])
  })

  test(`require deep-dir dir, no sub`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/deep-dir', false, /.js*/),
    })
    expect(Object.keys(requireList)).toStrictEqual([])
  })

  test(`require deep-dir dir, get childs`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/deep-dir', true, /.js*/),
    })
    expect(Object.keys(requireList)).toStrictEqual(['a', 'b', 'c'])
  })

  test(`require deep-dir dir, get childs, include:/^a$/`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/deep-dir', true, /.js*/),
      include: /^a$/,
    })
    expect(Object.keys(requireList)).toStrictEqual(['a'])
  })

  test(`require deep-dir dir, get childs, include:/^(a|b)$/`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/deep-dir', true, /.js*/),
      include: /^(a|b)$/,
    })
    expect(Object.keys(requireList)).toStrictEqual(['a', 'b'])
  })

  test(`require deep-dir dir, get childs, include:/^(a|b)$/ && exclude: ['a']`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/deep-dir', true, /.js*/),
      include: /^(a|b)$/,
      exclude: ['a'],
    })
    expect(Object.keys(requireList)).toStrictEqual(['b'])
  })

  test(`require index-dir dir`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/with-index-dir', true, /.js*/),
    })
    expect(Object.keys(requireList)).toStrictEqual(['a', 'b', 'c'])
  })

  test(`require with-_-dir dir`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/with-_-dir', true, /.js*/),
    })
    expect(Object.keys(requireList)).toStrictEqual(['a', 'b', 'c'])
  })

  test(`function type exclude`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/many-file', true, /.js*/),
      exclude: name => {
        if (name === 'a') return true

        return false
      },
    })
    expect(Object.keys(requireList)).toStrictEqual(['b', 'c'])
  })

  test(`function type include`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/many-file', true, /.js*/),
      include: name => {
        if (name === 'c') return true

        return false
      },
    })
    expect(Object.keys(requireList)).toStrictEqual(['c'])
  })

  test(`function type exclude, function type include`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/many-file', true, /.js*/),
      exclude: name => {
        if (name === 'a') return true

        return false
      },
      include: name => {
        if (name === 'c') return true

        return false
      },
    })
    expect(Object.keys(requireList)).toStrictEqual(['c'])
  })

  test(`require some dir with index.js`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/parent-dir', true, /.js*/),
    })
    expect(Object.keys(requireList)).toStrictEqual(['a', 'b', 'c'])
  })

  test(`require some dir with index.js`, () => {
    const requireList = eq.importAll({
      context: requireContext('./dir/parent-dir', true, /.js*/),
      key: ({ name }) => {
        if (name === 'a') return 'd'
      },
    })
    expect(Object.keys(requireList)).toStrictEqual(['d', 'b', 'c'])
  })
})
