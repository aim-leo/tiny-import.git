import requireContext from 'require-context.macro'
import { routeImport as eq } from '../../src/index'

describe('importRoute', () => {
  test('require route-dir, get route list', () => {
    const requireList = eq.importRoute({
      context: requireContext('./route-dir', true),
      value: ({ file }) => file,
      root: 'ChildA',
    })

    expect(requireList).toMatchObject([
      {
        path: '/',
        name: '/',
        component: {
          name: 'ChildA',
          components: {},
          watch: {},
          computed: {},
          methods: {},
          staticRenderFns: [],
        },
      },
      {
        path: '/childa',
        name: 'ChildA',
        component: {
          name: 'ChildA',
          components: {},
          watch: {},
          computed: {},
          methods: {},
          staticRenderFns: [],
        },
        children: [],
        _originPath: 'ChildA',
        _order: 0,
      },
      {
        path: '/childc',
        name: 'ChildC',
        component: {
          name: 'ChildC',
          components: {},
          watch: {},
          computed: {},
          methods: {},
          staticRenderFns: [],
        },
        children: [
          {
            path: 'subchilda',
            name: 'SubChildA',
            component: {
              name: 'SubChildA',
              components: {},
              watch: {},
              computed: {},
              methods: {},
              staticRenderFns: [],
            },
            children: [],
            _originPath: 'SubChildA',
            _order: 0,
          },
          {
            path: 'subchildb',
            name: 'SubChildB',
            component: {
              name: 'SubChildB',
              components: {},
              watch: {},
              computed: {},
              methods: {},
              staticRenderFns: [],
            },
            children: [],
            _originPath: 'SubChildB',
            _order: 0,
          },
          {
            path: ':id',
            name: 'DynamicSubChildC',
            component: {
              name: 'DynamicSubChildC',
              components: {},
              watch: {},
              computed: {},
              methods: {},
              staticRenderFns: [],
            },
            children: [],
            _originPath: ':id#DynamicSubChildC',
            _order: 1,
          },
          {
            path: '*',
            name: '@',
            component: {
              name: 'ChildNotFound',
              components: {},
              watch: {},
              computed: {},
              methods: {},
              staticRenderFns: [],
            },
            children: [],
            _originPath: '@',
            _order: 2,
          },
        ],
        _originPath: 'ChildC',
        _order: 0,
      },
      {
        path: '/:id',
        name: 'DynamicA',
        component: {
          name: 'DynamicA',
          components: {},
          watch: {},
          computed: {},
          methods: {},
          staticRenderFns: [],
        },
        children: [],
        _originPath: ':id#DynamicA',
        _order: 1,
      },
      {
        path: '*',
        name: '@',
        component: {
          name: 'NotFound',
          components: {},
          watch: {},
          computed: {},
          methods: {},
          staticRenderFns: [],
        },
        children: [],
        _originPath: '@',
        _order: 2,
      },
    ])
  })

  test('require route-dir, get sibling components list', () => {
    const requireList = eq.importComponent({
      context: requireContext('./component-dir', true),
      value: ({ file }) => file,
    })

    expect(Object.keys(requireList)).toMatchObject(['ComponentA', 'ComponentB'])

    expect(requireList).toMatchObject({
      ComponentA: {
        name: 'ComponentA',
        components: {},
        watch: {},
        computed: {},
        methods: {},
        staticRenderFns: [],
      },
      ComponentB: {
        name: 'ComponentB',
        components: {},
        watch: {},
        computed: {},
        methods: {},
        staticRenderFns: [],
      },
    })
  })
})
