import requireContext from 'require-context.macro'
import { tinyImport as eq } from '../../src/index'

describe('importJs', () => {
  test(`importJs export, get the es6 export`, () => {
    const requireList = eq.importJs({
      context: requireContext('./export', true),
    })

    expect(requireList).toEqual({ module: { a: 'a', b: 'b', default: 'c' } })
  })

  test(`importJs module_export, get the node export`, () => {
    const requireList = eq.importJs({
      context: requireContext('./module_export', true),
    })

    expect(requireList).toEqual({ module: { a: 1, b: 2 } })
  })
})
